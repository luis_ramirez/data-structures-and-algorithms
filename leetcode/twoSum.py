"""
    Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

    You may assume that each input would have exactly one solution, and you may not use the same element twice.

    You can return the answer in any order.

    Example 1:
    Input: nums = [2,7,11,15], target = 9
    Output: [0,1]
    Output: Because nums[0] + nums[1] == 9, we return [0, 1].

    Example 2:
    Input: nums = [3,2,4], target = 6
    Output: [1,2]

    Example 3:
    Input: nums = [3,3], target = 6
    Output: [0,1]
"""

def twoSum(nums, target):
    """
    :type nums: List[int]
    :type target: int
    :rtype: List[int]
    """
    for idx, number in enumerate(nums):
        searching_value = target - number
        try:
            expected_index = nums.index(searching_value, idx + 1)
            if expected_index:
                return [idx, expected_index]
        except:
            pass


def twoSum2(nums, target):
        seen = {}
        for i, v in enumerate(nums):
            remaining = target - v
            print(remaining)
            print(seen)
            if remaining in seen:
                return [seen[remaining], i]
            seen[v] = i
        return []
twoSum2([-1,-2,-3,-4,-5], -8)