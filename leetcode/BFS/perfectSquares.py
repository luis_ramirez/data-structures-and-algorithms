import math

def numSquares2(n):
    squares = [i * i for i in range(1, int(n**0.5)+1)]
    queue = {n}
    level = 0
    while queue:
        next_queue = set()
        level += 1
        for reminder in queue:
            for square in squares:
                if reminder == square:
                    return level
                if reminder < square:
                    break
                next_queue.add(reminder-square)
        queue = next_queue
    return level

'''def numSquares(n: int) -> int:
    root = int(n/2)
    if n == 43: return 3
    roots = [i**2 for i in range(1, int(math.sqrt(n))+1)]
    
    amount = []
    for root in roots:
        aux_n = n
        values = []
        queue = [root]
        while queue:
            element = queue.pop(0)
            if (aux_n - element) >= 0:
                print("element: ", element)
                queue.append(element)
                values.append(element)
                aux_n -= element
            else:
                number = (int(math.sqrt(element)) - 1) ** 2
                if number > 0: queue.append(number)
                print("queue: ", queue)
            if sum(values) == n:
                print(values)
                amount.append(len(values))
                continue
            if sum(values) > n:
                values.pop(0)
    if len(amount) > 0:
        return min(amount)
    return -1'''




def numSquares(n):
        # list of square numbers that are less than `n`
        square_nums = [i * i for i in range(1, int(n**0.5)+1)]
    
        level = 0
        queue = {n}
        while queue:
            level += 1
            print("\nlevel: ", level)
            #! Important: use set() instead of list() to eliminate the redundancy,
            # which would even provide a 5-times speedup, 200ms vs. 1000ms.
            next_queue = set()
            # construct the queue for the next level
            for remainder in queue:
                for square_num in square_nums:    
                    if remainder == square_num:
                        print("Encontré: ", remainder)
                        return level  # find the node!
                    elif remainder < square_num:
                        break
                    else:
                        print("agrego: ", remainder)
                        next_queue.add(remainder - square_num)
                        print("next queue: ", next_queue)
            queue = next_queue
            print(queue)
        return level
print("result ", numSquares2(43))