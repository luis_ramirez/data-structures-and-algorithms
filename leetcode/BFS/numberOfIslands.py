'''
Given an m x n 2d grid map of "1"s (land) and "0"s (water), return the number of islands.
An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically.
You may assume all four edges of the grid are all surrounded by water.

Example 1:
Input: grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
Output: 1

Example 2:
Input: grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
Output: 3

Constraints:

    m == grid.length
    n == grid[i].length
    1 <= m, n <= 300
    grid[i][j] is "0" or "1".
'''

def numIslands(grid):
    count = 0
    for row in range(len(grid)):
        for col in range(len(grid[row])):
            if grid[row][col] == '1':
                part_of_island(row, col, grid)
                count += 1
    return count

def part_of_island(row, col, grid):
    if (row >= len(grid) or row < 0 or col >= len(grid[row])
        or col < 0 or grid[row][col] != '1'):
        return
    
    grid[row][col] = '0'
    part_of_island(row - 1, col, grid)
    part_of_island(row + 1, col, grid)
    part_of_island(row, col - 1, grid)
    part_of_island(row, col + 1, grid)

print(numIslands([
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]))