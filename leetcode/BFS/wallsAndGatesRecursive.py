"""
You are given a m x n 2D grid initialized with these three possible values.

    -1 - A wall or an obstacle.
    0 - A gate.
    INF - Infinity means an empty room. We use the value 231 - 1 = 2147483647 to represent INF as you may assume that the distance to a gate is less than 2147483647.

Fill each empty room with the distance to its nearest gate. If it is impossible to reach a gate, it should be filled with INF.

Example: 

Given the 2D grid:

INF  -1  0  INF
INF INF INF  -1
INF  -1 INF  -1
  0  -1 INF INF

After running your function, the 2D grid should be:

  3  -1   0   1
  2   2   1  -1
  1  -1   2  -1
  0  -1   3   4

TestCase
[[2147483647,-1,0,2147483647],[2147483647,2147483647,2147483647,-1],[2147483647,-1,2147483647,-1],[0,-1,2147483647,2147483647]]

func wallsAndGates(rooms [][]int) {
	for i := 0; i < len(rooms); i++ {
		for j := 0; j < len(rooms[0]); j++ {
			if rooms[i][j] == 0 {
				// to speed up the calculation
				// instead of calculating from the INF, calculating from 0 can avoid redundant calculation
				dfs(rooms, i, j, 0)
			}
		}
	}
}

func dfs(rooms [][]int, i int, j int, steps int) {
	if i < 0 || i+1 > len(rooms) || j < 0 || j+1 > len(rooms[0]) {
		return
	}
	if rooms[i][j] == -1 {
		return
	}
	// if the "steps" is calculated, dont need to calculate again
	if steps <= rooms[i][j] {
		rooms[i][j] = steps
		dfs(rooms, i-1, j, steps+1)
		dfs(rooms, i+1, j, steps+1)
		dfs(rooms, i, j-1, steps+1)
		dfs(rooms, i, j+1, steps+1)
	}
}
"""