"""
You are given a m x n 2D grid initialized with these three possible values.

    -1 - A wall or an obstacle.
    0 - A gate.
    INF - Infinity means an empty room. We use the value 231 - 1 = 2147483647 to represent INF as you may assume that the distance to a gate is less than 2147483647.

Fill each empty room with the distance to its nearest gate. If it is impossible to reach a gate, it should be filled with INF.

Example: 

Given the 2D grid:

INF  -1  0  INF
INF INF INF  -1
INF  -1 INF  -1
  0  -1 INF INF

After running your function, the 2D grid should be:

  3  -1   0   1
  2   2   1  -1
  1  -1   2  -1
  0  -1   3   4

TestCase
[[2147483647,-1,0,2147483647],[2147483647,2147483647,2147483647,-1],[2147483647,-1,2147483647,-1],[0,-1,2147483647,2147483647]]
"""

class Solution(object):
    def wallsAndGates(self, rooms):
        """
        :type rooms: List[List[int]]
        :rtype: None Do not return anything, modify rooms in-place instead.
        """
        if len(rooms) <= 0:
            return
        DIRECTIONS = [[1,0], [-1,0], [0,1], [0,-1]]
        rows_length = len(rooms)
        cols_length = len(rooms[0])
        queue = []
        for row in range(0, rows_length):
            for col in range(0, cols_length):
                if rooms[row][col] == 0:
                    queue.append([row, col])
        while queue != []:
            current = queue.pop(0)
            for direction in DIRECTIONS:
                x_aux = current[0] + direction[0]
                y_aux = current[1] + direction[1]
                if (x_aux < 0 or y_aux < 0 or x_aux >= rows_length
                    or y_aux >= cols_length or rooms[x_aux][y_aux] != 2147483647):
                    continue
                rooms[x_aux][y_aux] = rooms[current[0]][current[1]] + 1
                queue.append([x_aux, y_aux])