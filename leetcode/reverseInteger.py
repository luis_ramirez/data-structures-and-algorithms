"""
    Given a 32-bit signed integer, reverse digits of an integer.

    Note:
    Assume we are dealing with an environment that could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

    Example 1:

    Input: x = 123
    Output: 321

    Example 2:

    Input: x = -123
    Output: -321

    Example 3:

    Input: x = 120
    Output: 21

    Example 4:

    Input: x = 0
    Output: 0
"""
def reverse(self, x):
    """
    :type x: int
    :rtype: int
    """
    new = ""
    num_str = str(x)
    if x <= 2147483647 and x > -2147483647:
        
        for num in range(0, len(num_str)):
            new += num_str[-(1 + num)]
        
        if new[-1] == "-":
            new = int(new[:-1]) * -1
        else:
            new = int(new)
        if new <= 2147483647 and new > -2147483647:
            return new
        else: return 0
    else:
        return 0

def reverse2(self, x):
    s = str(x)[::-1]
    result = int(s) if s[-1] != '-' else int(s[:-1]) * -1
    if result >= -2**31 and result <= (2**31)-1:
        return result
    else:
        return 0