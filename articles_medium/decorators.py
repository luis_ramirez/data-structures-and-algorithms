# First class order function example
""""def say_hi():
    print('Hi!')
greeting = say_hi()
greeting()"""

def outer_function(msg):
    message = msg
    def inner_function():
        print(message)
    return inner_function

hola = outer_function('hola')
adios = outer_function('adios')
hola()
adios()

def decorator_function(original_function):
    def wrapper_function():
        print('parameter function being executed: {}'.format(original_function.__name__))
        original_function()
    return wrapper_function

def say_hello():
    print('Hello!')

decorated_function = decorator_function(say_hello)
decorated_function()  

@decorator_function
def say_hello():
    print('Hello!')
say_hello()
