def riot_login(driver):
    try:
        explore_bar = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//a[@id='riotbar-explore']"))
        )
        explore_bar.click()
        login_button = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='riotbar-navmenu-category']"))
        )
        '''time.sleep(2) # Let the user actually see something!
        login_button = driver.find_element(By.CSS_SELECTOR("a[data-riotbar-link-id='login']"))'''
        login_button.click()
        
        username_input =  WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//input[@name='username']"))
        )
        username = input("Ingresa tu usuario username: ")
        username_input.send_keys(username)
        password_input = driver.find_element(By.XPATH, "//input[@name='password']")
        password = input("Ingresa tu contraseña: ")
        password_input.send_keys(password)
        submit_login_button = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//button[@class='mobile-button mobile-button__submit']"))
        )
        submit_login_button.click()
        login_error_message = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.XPATH, "//span[@class='status-message text__web-error']"))
        )
        if login_error_message:
            print("Hubo un problema iniciando sesión, revisa tu usuario o contraseña de Riot")
            driver.quit()
    except ValueError as err:
        driver.quit()

def check_game(driver):
    try:
        '''
        Revisamos que no esté en este momento el juego
        '''
        element = WebDriverWait(driver, 5).until(
            EC.element_to_be_clickable((By.XPATH, "//a[@href='/live/worlds']"))
        )    
        element.click()
        sys.exit()
    except:
        pass

if __name__ == "__main__":
    # get an instance of a the web driver, it could be other like firefox 
    driver = webdriver.Chrome() 
    # seting the entrypoint where we're going to work
    driver.get('https://lolesports.com/')
    hora_inicio = 0
    riot_login(driver)
    try:
        explore_bar = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//a[@id='riotbar-explore']"))
        )
        explore_bar.click()
        calendar_button = driver.find_element_by_link_text('CALENDARIO')
        calendar_button.click()
    except:
        driver.quit()
    check_game(driver)
    print("Cuando es el juego? Hoy en unas horas o mañana por la madrugada? Generalmente los juegos son a las 5am hora méxico")
    print("Escribe Hoy si es después de las 12 am del día del juego, si no escribe Mañana")
    cuando = input("Cuando será el siguiente juego, Hoy o Mañana?")
    event_dates = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.XPATH, "//div[@class='Event']"))
    )
    event_divs = event_dates.find_elements_by_xpath("//div[starts-with(@class,'Event')]")
    for idx, element in enumerate(event_divs):
        try:
            if element.find_element_by_class_name("weekday").text == cuando:
                hora_inicio = int(event_divs[idx+1].find_element_by_class_name("hour").text)
        except:
            pass
    if hora_inicio != 0:
        sched = BlockingScheduler(daemon=True)
        if cuando == "Hoy": 
            hora_inicio = datetime.now().replace(hour=hora_inicio, minute=10)
        else: cuando == "Mañana":
            hora_inicio = datetime.now().replace(hour=hora_inicio, minute=10) + timedelta(days=1)
        sched.add_job(check_game, 'date', run_date=hora_inicio, kwargs={'driver': driver})
        sched.start()