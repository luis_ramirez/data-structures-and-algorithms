def recursive_fib(number):
    # base case fib(0) = 0
    if number == 0:
        return 0
    # second base case where the previous numbers gives 0
    # fib(1) = 1 fib(2) = 1
    elif number == 1 or number == 2:
        return 1
    else:
        hola = recursive_fib(number - 1) + recursive_fib(number - 2)
        return hola
print(recursive_fib(9))
print(recursive_fib(11))
print(recursive_fib(0))