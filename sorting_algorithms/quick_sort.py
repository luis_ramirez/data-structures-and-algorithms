def quick_sort_recursive(array):
    if len(array) <= 1:
        return array
    else:
        # the last array element is our pivot
        pivot = array[-1]
        # for all elements lower tha the pivot
        left_array = []
        # for all elements greather tha the pivot
        right_array = []
        for element in array[:-1]:
            if element <= pivot: left_array.append(element)
            elif element > pivot: right_array.append(element)
        return quick_sort_recursive(left_array) + [pivot] + quick_sort_recursive(right_array)

test = [21, 4, 1, 3, 9, 20, 25, 6, 21, 14]
print(quick_sort_recursive(test))

"""def partition(array, low, high):
    pivot = array[high]
    smaller_element = array[low] - 1
    iterator = low
    while iterator <= high:
        if array[iterator] <= pivot:
            smaller_element += 1
            array[iterator], array[smaller_element] = array[smaller_element], array[iterator]
        iterator += 1
    array[high], array[smaller_element] = array[smaller_element + 1], array[high]
    return(smaller_element + 1)

def quick_sort(array, low, high):
    if (low < high):
        middle_element = partition(array, low, high)
        quick_sort(array, low, middle_element - 1)
        quick_sort(array, middle_element + 1, high)

print(print(quick_sort(test, 0, len(test) - 1)))"""
